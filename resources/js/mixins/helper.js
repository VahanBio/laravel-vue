import store from '../store';
import moment from "moment";
import {$http} from "../api/api";
import {api_url} from "../constants/api";
import $ from "jquery";

export default {
    methods: {
        $User() {
            return store.getters.getUser;
        },
        $HasError(errors, field){
            return errors[field] !== undefined;
        },
        $GetError(errors, field){
            return errors[field][0];
        },
        $Date(date, format){
            return moment(date).format(format) === 'Invalid date' ? null : moment(date).format(format);
        },
        $FullName(user){
            if (user['first_name'] || user['last_name']) {
                return user['first_name'] + ' ' + user['last_name'];
            } else {
                return '';
            }
        },
        $Upload(file, Editor, cursorLocation, resetUploader){
            let formData = new FormData();
            formData.append("image", file);

            store.commit('setPreloader', true);
            $http().post(api_url + 'upload', formData).then(({data}) => {
                if (data.success){
                    store.commit('setUser', data.user);
                    Editor.insertEmbed(cursorLocation, "image", data.path);
                    resetUploader();
                } else {
                    this.$notify.error({
                        title: 'Something went wrong',
                        message: data.error
                    });
                }
                store.commit('setPreloader', false);
            }).catch(() => {
                store.commit('setPreloader', false);
            });
        },
        $TogglePopup(name){
            let popups = store.getters.getPopups;
            let body = document.body;
            for (let key in popups){
                if (popups.hasOwnProperty(key)){
                    popups[key] = key === name ? !popups[key] : false;
                }
            }
            store.commit('setPopups', popups);
            body.classList.add('disable-scroll');
        },
        $NotifyErrors(errors){
            for (let key in errors){
                if (errors.hasOwnProperty(key)){
                    key = key.split('_').join(' ');
                    setTimeout(() => {
                        this.$notify.error({
                            title: key.charAt(0).toUpperCase() + key.slice(1),
                            message: errors[key][0]
                        });
                    }, 1);
                }
            }
        },
        $ClosePopup(){
            if ($(this.target).parents('.popups-background').length === 0){
                this.$TogglePopup(undefined);
            }
            document.body.classList.remove('disable-scroll');
        },
    }
};