import VueRouter from 'vue-router';
import store from '../store';
import moment from "moment";
import {$http} from "../api/api";
import {api_url} from "../constants/api";

import routes from './routes';

const router = new VueRouter({
    routes,
    linkExactActiveClass: 'active',
    mode: 'history'
});

router.beforeEach(async (to, from, next) => {
    let user = null;

    let clearStorage = () => {
        store.commit("setAccessToken", null);
        store.commit("setExpiresAt", null);
        store.commit("setIsLogged", false);
        store.commit("setUser", null);
        next('/');
    };

    if (store.getters.getIsLogged) {
        let now = moment().format('YYYY-MM-DD HH:mm:ss');
        let expires_at = moment(store.getters.getExpiresAt);
        let is_expired = expires_at.diff(now) <= 0;

        if (is_expired) {
            clearStorage();
        } else {
            await $http().post(api_url + "me").then(({data}) => {
                if (data.success) {
                    user = data.user;
                    store.commit('setUser', user);
                } else {
                    clearStorage();
                }
            }).catch(() => {
                clearStorage();
            });
        }
    }

    if (to.meta['auth'] === 'guest') {
        if (user) {
            next('/');
        } else {
            next();
        }
    } else if (to.meta['auth'] === 'auth') {
        if (user) {
            next();
        } else {
            next('/');
        }
    } else {
        next();
    }
    window.scrollTo({
        top: 0,
        behavior: 'smooth'
    });
});

export default router;