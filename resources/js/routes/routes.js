import Home from "../views/Home";
import About from "../views/About";

import NotFound from "../components/NotFound";

const routes = [
    {
        name: 'home',
        path: '/',
        component: Home,
        meta: {auth: 'each', title: 'Home'}
    },
    {
        name: 'about',
        path: '/about',
        component: About,
        meta: {auth: 'each', title: 'About'}
    },
    {
        name: 'not-found',
        path: "/:catchAll(.*)",
        component: NotFound,
        meta: {auth: 'each'}
    },
];
export default routes