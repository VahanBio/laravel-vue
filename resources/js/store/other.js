export default {
    state: {
        popups: {
            login: false,
        },
        sidebar: false,
        preloader: false
    },
    mutations: {
        setPopups(state, payload) {
            state.popups = payload;
        },
        setSidebar(state, payload) {
            state.sidebar = payload;
        },
        setPreloader(state, payload) {
            state.preloader = payload;
        }
    },
    getters: {
        getPopups(state) {
            return state.popups;
        },
        getSidebar(state) {
            return state.sidebar;
        },
        getPreloader(state) {
            return state.preloader;
        }
    }
};
