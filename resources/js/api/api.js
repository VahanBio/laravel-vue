import axios from "axios";
import store from "../store";
import {Notification} from "element-ui";

export const $http = () => {
    axios.defaults.headers['common']['Accept'] = 'application/json';
    axios.defaults.headers['common']['Authorization'] = 'Bearer ' + store.getters.getAccessToken;
    axios.defaults.headers['common']['Content-Type'] = 'multipart/form-data';

    axios.interceptors.response.use(response => {
        return response;
    }, error => {
        const status = error.response && error.response.status ? error.response.status : 0;

        if (status === 401) {
            Notification({
                title: 'Unauthenticated.',
                type: 'warning'
            });
        }

        if (status === 404) {
            Notification({
                title: 'Oops! Please reload the page.',
                type: 'warning'
            });
        }

        if (status === 405) {
            Notification({
                title: 'Access denied.',
                type: 'warning'
            });
        }

        if (status === 500) {
            Notification({
                title: 'There has been server failure.',
                type: 'error'
            });
        }

        return error;
    });

    return axios;
};
