import Vue from 'vue';
import App from './App.vue';

import store from './store';
import router from './routes';

import VueRouter from 'vue-router';
import Vuelidate from 'vuelidate';
import ElementUI from 'element-ui';
import uiEn from 'element-ui/lib/locale/lang/en';
import onlyInt from 'vue-input-only-number';
import 'bootstrap';

Vue.use(VueRouter);
Vue.use(Vuelidate);
Vue.use(ElementUI, {locale: uiEn});
Vue.use(onlyInt);

new Vue({
    store,
    router,
    render: app => app(App)
}).$mount('#app');
